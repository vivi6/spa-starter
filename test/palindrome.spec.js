import { Palindrome } from "../src/palindrome";
describe ("Palindrome", () => {

    let palindrome;

    beforeEach(()=>{
        palindrome = new Palindrome();
    })

    it('given a palindrome letter with format correct', () => {
        const phrase = "oso"
        expect(palindrome.isPalindrome(phrase)).toEqual("is palindrome");
    })

    it('given a palindrome phrase with format correct', () => {
        const phrase = "Logra Casillas alli sacar gol"
        expect(palindrome.isPalindrome(phrase)).toEqual("is palindrome");
    })

    it('given a palindrome phrase with format correct and comma', () => {
        const phrase = "Yo dono rosas, oro no doy"
        expect(palindrome.isPalindrome(phrase)).toEqual("is palindrome");
    })

    it('given a phrase with palindrome format incorrect', () => {
        const phrase = "Cualquier cosa"
        expect(palindrome.isPalindrome(phrase)).toEqual("is not palindrome");
    })


})