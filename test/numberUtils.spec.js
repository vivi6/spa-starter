import { NumberUtils } from "../src/numberUtils";
describe ("Number Utils", () => {

    let number;

    beforeEach(()=>{
        number = new NumberUtils();
    })

    it('given a number is equal to 5 should return Is odd', () => {
        expect(number.isEvenOrOdd(5)).toEqual("Is odd");
    })

    it('given a number is equal to 4 should return Is even', () => {
        expect(number.isEvenOrOdd(4)).toEqual("Is even");
    })

    it('given a number is equal to -6 should return Is even', () => {
        expect(number.isEvenOrOdd(-6)).toEqual("Is even");
    })

    it('given a number is equal to -1 should return Is odd', () => {
        expect(number.isEvenOrOdd(-1)).toEqual("Is odd");
    })


})