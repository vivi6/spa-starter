import { Calculadora } from "../src/calc";
describe ("Calculadora", () => {

    let calc;

    beforeEach(()=>{
        calc= new Calculadora();
    })

    it('sum', () => {
        const a = 1;
        const b = 3;

        //se puede tener aqui, pero es para ver el uso del beforeEach
        // const calc = new Calculadora();

        expect(calc.sum(a,b)).toEqual(4);
    })

})