import { NIF } from "../src/NIF";
describe ("Validate NIF", () => {

    let nif;

    beforeEach(()=>{
        nif = new NIF();
    })

    it('given a dni with format valid', () => {
        const dni = "45777331V"
        const result = {
            message: "Format validated",
            error: ""
        }
        expect(nif.validate(dni)).toEqual(result);
    })

    it('given a dni with format invalid', () => {
        const dni = "31VSFSFWE"
        const result = {
            message: "Format invalidate",
            error: "the DNI format must be 12345678X"
        }
        expect(nif.validate(dni)).toEqual(result);
    })

    it('given a dni with length different from 9', () => {
        const dni = "457779844331V"
        let result = {
            message: "Format invalidate",
            error: "the length of DNI must be 9"
        }
        expect(nif.validate(dni)).toEqual(result)
    })

    it('given a dni with letter incorrect', () => {
        const dni = "54585738V"
        let result = {
            message: "Format invalidate",
            error: "the letter is incorrect"
        }
        expect(nif.validate(dni)).toEqual(result)
    })


  

})