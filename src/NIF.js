export class NIF {

    result = {
        message: "",
        error: ""
    }

    lettersDni = ['T', 'R', 'W', 'A', 'G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E']

    validate(value) {
        var nifRegex = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]$/i;
        

        if( value.length != 9){
            this.result.message = "Format invalidate"
            this.result.error = "the length of DNI must be 9"
            return this.result
        }

        if( !nifRegex.test(value) ){
            this.result.message = "Format invalidate"
            this.result.error = "the DNI format must be 12345678X"
            return this.result
        }

        if( nifRegex.test(value) ){

            var dni = value.toString().toUpperCase();
            var numberDni = dni.substring(0, 8) % 23
            var isValidDniLetter = this.lettersDni[numberDni] == dni.substring(8,9);
            
            if( isValidDniLetter ){
                this.result.message = "Format validated"
                this.result.error = ""
                return this.result
            }else{
                this.result.message = "Format invalidate"
                this.result.error = "the letter is incorrect"
                return this.result
            }
        }
    }
}